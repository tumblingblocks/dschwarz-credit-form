var express = require('express');
var app = express();
var bodyParser = require('body-parser');
var validator = require('node-validator');

//bodyParser to allow app to get data from POST
app.use(bodyParser.urlencoded({ extended: true }));
app.use(bodyParser.json());

//Allow CORS
var allowCrossDomain = function(req, res, next) {
  res.header('Access-Control-Allow-Origin', 'http://localhost:8001');  
  res.header('Access-Control-Allow-Methods', 'GET,PUT,POST,OPTIONS');
  res.header('Access-Control-Allow-Headers', 'Authorization, Content-Length, X-Requested-With, Origin, Accept, Content-Type');
  
  next();
  
};

app.use(allowCrossDomain);

// set up node-validator validations
var checkSubmitted = validator.isObject()
	.withRequired('firstName')
	.withRequired('lastName')
	.withRequired('address')
	.withRequired('ssn', validator.isString({ regex: /^(?!123456789|987654321)^(\d)(?!\1{8}$)\d{8}$/}))
	.withRequired('email')
	.withRequired('emailConfirmation')
	.withRequired('submitted-at')

//create API route for POST applicants
var router = express.Router();

router.route('/application')
	.post([validator.express(checkSubmitted), function(req,res) {
		var errorState = false;
		var errors = [];

		//validate email match
		if (req.body['email'] && req.body['emailConfirmation'] && req.body['email'] !== req.body['emailConfirmation']){
			errorState = true;
			errors.push({"message": "Email and Email Confirmation must match."});
		}

		// TODO: move to custom validator
		if (req.body['address']) {
			var a = req.body['address'];
			// make sure we have a string and send it to lowercase for matching
			a = a.toString().toLowerCase();
			// get rid of periods for matching
			a = a.replace(/\./g,'');
			// reject if there is a space between 'po' and 'box', and 'po' is either at the beginning or has a space in front
			// allow for sentences containing 'pobox' or even '...po box..' in the middle.
			if (/^(po box)|( po box)/.test(a)) {
				errors.push({"message": "Address can not contain P.O. Box."});
				errorState = true;
			}
		}
		
		//accept or reject with status
		if (errorState) {
			res.status(400).send({"errors": errors});
		} else {
			res.status(201).send({"message": "success"});
		}


	}])

app.use('/api', router);
 
app.listen(8080)
console.log('API available on port 8080');
