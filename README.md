# Credit Application 

## A sample app

### To run the app

You will need to have node and npm installed.

After cloning the repo, cd into the api project folder, "credit-api"

Run `npm install`

Then start the backend with:

```node server.js```

Your api will be available on port 8080

Next, in another terminal window, change to the "credit-ui" folder where the front end project is.

run `npm install` to get packages used for testing and development.

Start the front end by running:

```gulp```

The web app will be available at http://localhost:8001/.

If you would like to run the tests, in a different terminal window run:

```gulp test```

### About

This project has a node.js backend and a JavaScript (ES5) front end.
It uses gulp as a front end build tool.
It uses twitter Bootstrap as a quick UI.

Repo available at https://bitbucket.org/tumblingblocks/dschwarz-credit-form.