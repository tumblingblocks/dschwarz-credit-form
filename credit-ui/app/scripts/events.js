// events.js
// form interaction events

var creditApp = creditApp || {};

//for reacting to from and user events

creditApp.events = {

	//default form state
	submittedState: false,

	checkAddress: function(value, id) {
		this.handleErrorDisplay(id, creditApp.validations.validatePoBox(value));
	},

	checkEmails: function(value1, value2, id, id2) {
		var emailsValid = creditApp.validations.validateEmails(value1, value2);
		this.handleErrorDisplay(id, emailsValid);
		this.handleErrorDisplay(id2, emailsValid);
	},

	checkSsn: function(value1, id) {
		this.handleErrorDisplay(id, creditApp.validations.validateSsn(value1));
	},

	checkRequired: function(value, id) {
		this.handleErrorDisplay(id, creditApp.validations.validateRequired(value));
	},

	applyInputToBlur: function() {
		var fields = document.getElementsByTagName('input');
			for (var i=0; i<fields.length; i++) { 
				fields[i].onblur = fields[i].oninput;
			}
	},

	// for non-required errors
	handleErrorDisplay: function(id, errorData) {
		var element = document.getElementById(id);
		if (errorData.valid) {
			element.parentNode.className ='form-group';
			element.nextElementSibling.className ='error-message';
			element.nextElementSibling.innerText = '';
		} else {
			element.parentNode.className='form-group has-error';
			element.nextElementSibling.className ='error-message text-danger';
			element.nextElementSibling.innerText = errorData.message;
		}
		this.countErrors();
	},

	countErrors: function() {
		var fields = document.getElementsByTagName('input');
		var errorCount = 0;

		for (var i=0; i<fields.length; i++) { 
			if (fields[i].parentNode.className==='form-group has-error') {
				errorCount+=1;
			}
		}
		
		if (this.submittedState) {
			document.querySelector('.errorCount').innerHTML = errorCount + (errorCount > 1 ? ' errors' : ' error');
		}
		if (errorCount === 0){
			document.querySelector('.errorCount').innerHTML = '';
		}
		
		return errorCount;
	},

	checkRequiredErrors: function() {
		var fields = document.getElementsByTagName('input');

		for (var i=0; i<fields.length; i++) { 
			if (fields[i].value==='') {
				fields[i].parentNode.className='form-group has-error';
				fields[i].nextElementSibling.className='error-message text-danger';
				fields[i].nextElementSibling.innerText='Required';
			}
		}

	},

	submitApplication: function(e) {
		e.preventDefault();

		this.submittedState = true;
		this.checkRequiredErrors();
		var errorCount = this.countErrors();

		if (errorCount === 0) {
			// disable submit
			document.getElementById('submit').setAttribute('disabled', true);
			// send request
			creditApp.applicant.sendApplicant();

		}
		
		
	}

};