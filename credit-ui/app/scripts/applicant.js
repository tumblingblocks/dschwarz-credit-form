// applicant.js
// handles working with the applicant data object

var creditApp = creditApp || {};

creditApp.applicant = {
	// set defaults
	applicantData: {
		firstName: '',
		lastName: '',
		address: '',
		ssn: '',
		email: '',
		emailConfirmation: ''
	},

	sendApplicant: function(){
		// get data from form
		for (var key in this.applicantData) {
			if (document.getElementById(key)){
				this.applicantData[key] = document.getElementById(key).value;
			}
		}

		this.applicantData.ssn = creditApp.validations.sanitizeSsn(this.applicantData.ssn);

		var submitDate = new Date();
		this.applicantData['submitted-at'] = submitDate.toDateString() + ' ' + submitDate.toLocaleTimeString();

		//set up request
		xhr = new XMLHttpRequest();
		var url = 'http://localhost:8080/api/application';
		xhr.open('POST', url, true);
		xhr.setRequestHeader('Content-type', 'application/json');
		xhr.onreadystatechange = this.handleResponse;
		var data = JSON.stringify(this.applicantData);
		xhr.send(data);
	},

	handleResponse: function() {
		if (xhr.readyState === 4){
			if (xhr.status === 201) { // success
				document.querySelector('.alert').className = 'alert alert-success';
			  document.querySelector('.alert').innerHTML = 'Thank you. Your credit application was completed successfully.';
				
			} else if ( xhr.status === 400) { // bad request, give feedback
				var errorResponse = JSON.parse(xhr.responseText);

				//show individual errors
				var errorMessage = '';
				document.querySelector('.alert').className = 'alert alert-danger';
				for (var e=0; e<errorResponse.errors.length; e++ ){
					errorMessage = errorMessage + '<br/> ' + errorResponse.errors[e].parameter + ': ' + errorResponse.errors[e].message;
				}

			  document.querySelector('.alert').innerHTML = 'There was a problem with your submission. ' + errorMessage;

			} else { // random error
				document.querySelector('.alert').className = 'alert alert-danger';
			  document.querySelector('.alert').innerHTML = 'There was a problem with your submission.';
			}
			// re-enable submit
			document.getElementById('submit').removeAttribute('disabled');

		}
	}

};

