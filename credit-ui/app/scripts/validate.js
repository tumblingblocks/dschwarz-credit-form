// validate.js
// form field validations

var creditApp = creditApp || {};

creditApp.validations = {

	// email and emailConfirmation must match
	validateEmails: function(email1, email2){
		var errorData = {
			valid: true,
			message: 'Email and Email Confirm must match.'
		};
		if ((email1 && email2) && email1 !== email2){
			errorData.valid = false;
		}
		if (!email1 || !email2){
			errorData.valid = false;
			errorData.message = 'Email and Email Confirmation are required';
		}
		return errorData;
	},

	validateSsn: function(ssn){
		var errorData = {
			valid: true,
			message: 'Must be a valid SSN.'
		};
		ssn = ssn.toString();
		// strip out any spaces or dashes.  those are reasonably ok parts of a ssn
		ssn = this.sanitizeSsn(ssn);
		// Now that we've removed acceptable non-numbers, validate the rest
		// reject all of the same digit ^(\d)(\1{8}$)$
		// disallow blacklisted numbers (?!123456789|987654321)
		// match 9 digits (8 after first match) \d{8}
		if (!/^(?!123456789|987654321)^(\d)(?!\1{8}$)\d{8}$/.test(ssn)){
			errorData.valid = false;
		}
		if (!ssn){
			errorData.valid = false;
			errorData.message = 'Required';
		}
		return errorData;
	},

	sanitizeSsn: function(ssn) {
		return ssn.replace(/[- ]/g, '');
	},

	validatePoBox: function(a){
		var errorData = {
			valid: true,
			message: 'Can not contain a P.O. Box'
		};
		// make sure we have a string and send it to lowercase for matching
		a = a.toString().toLowerCase();
		// get rid of periods for matching
		a = a.replace(/\./g,'');
		// reject if there is a space between 'po' and 'box', and 'po' is either at the beginning or has a space in front
		// allow for sentences containing 'pobox' or even '...po box..' in the middle.
		if (/^(po box)|( po box)/.test(a)) {
			errorData.valid = false;
		}
		if (!a || a ===''){
			errorData.valid = false;
			errorData.message = 'Required';
		}
		return errorData;
	},

	validateRequired: function(val){
		var errorData = {
			valid: true,
			message: 'Required'
		};
	
		if (!val){
			errorData.valid = false;
			errorData.message = 'Required';
		}
		return errorData;
	}

};

