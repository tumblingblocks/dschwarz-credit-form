var gulp = require('gulp'),
jshint = require('gulp-jshint');
var Server = require('karma').Server;
var connect = require('gulp-connect');


gulp.task('connect', function () {
  connect.server({
    root: 'app',
    port: 8001
  });

  gulp.watch('app/scripts/*.js', ['jshint']);
});

gulp.task('default', ['connect']);


// configure the jshint task
gulp.task('jshint', function() {
  return gulp.src('app/scripts/*.js')
    .pipe(jshint())
    .pipe(jshint.reporter('jshint-stylish'));
});

gulp.task('test', function(done){
	new Server({
		configFile: __dirname + '/karma.conf.js'
	}, done).start();
});


// configure which files to watch and what tasks to use on file changes
gulp.task('watch', function() {
  gulp.watch('app/scripts/*.js', ['jshint']);
});