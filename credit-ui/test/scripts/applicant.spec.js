describe('Applicant', function(){

	var applicant;

	// abbreviate module
	beforeEach(function() {
    applicant = creditApp.applicant;
  });

	it ('should construct a base applicant', function(){
		expect(applicant.applicantData.firstName).toBe('');
		expect(applicant.applicantData.lastName).toBe('');
		expect(applicant.applicantData.address).toBe('');
		expect(applicant.applicantData.ssn).toBe('');
		expect(applicant.applicantData.email).toBe('');
		expect(applicant.applicantData.emailConfirmation).toBe('');

	});


});