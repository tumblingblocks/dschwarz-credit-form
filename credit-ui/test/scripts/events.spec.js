describe('Events', function(){

	var val;

	// abbreviate module
	beforeEach(function() {
    events = creditApp.events;


  });

	it ('should run address validations and call display update', function(){

		spyOn(events,'handleErrorDisplay');

		events.checkAddress('value', 'id');

		expect(events.handleErrorDisplay).toHaveBeenCalled();

	});

	it ('should run email validations and call display update', function(){

		spyOn(events,'handleErrorDisplay');

		events.checkEmails('value', 'id', 'value2', 'id2');

		expect(events.handleErrorDisplay).toHaveBeenCalled();

	});

	it ('should run ssn validations and call display update', function(){

		spyOn(events,'handleErrorDisplay');

		events.checkSsn('value', 'id');

		expect(events.handleErrorDisplay).toHaveBeenCalled();

	});

	it ('should run PO Box validations and call display update', function(){

		spyOn(events,'handleErrorDisplay');

		events.checkAddress('value', 'id');

		expect(events.handleErrorDisplay).toHaveBeenCalled();

	});

	// TODO
	// admittedly, I'm gotten a bit hung up wanting to test more here, but having to deal wit DOM structure
	// I think I'd need some sort of mock dom, but not sure I'll get there in time.


});