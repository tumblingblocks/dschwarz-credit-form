describe('Validation for credit application fields', function(){

	var val;

	// abbreviate module
	beforeEach(function() {
    val = creditApp.validations;
  });

	it ('email and confirm email should match', function(){

		expect(val.validateEmails('test@company.com', 'test1@company.com')).toEqual({ valid: false, message: 'Email and Email Confirm must match.' });
		expect(val.validateEmails('test@company.com', 'test@company.com')).toEqual({ valid: true, message: 'Email and Email Confirm must match.' });

	});

	it ('should check that SSN is valid', function(){
		var correctFalse = { valid: false, message: 'Must be a valid SSN.' };
		expect(val.validateSsn('123-45678')).toEqual(correctFalse);
		expect(val.validateSsn('111 11 1111')).toEqual(correctFalse);
		expect(val.validateSsn('c11 11 1111')).toEqual(correctFalse);
		expect(val.validateSsn('111 11 1111cats')).toEqual(correctFalse);
		expect(val.validateSsn('1234567899')).toEqual(correctFalse);
		expect(val.validateSsn('alphastring')).toEqual(correctFalse);
		expect(val.validateSsn(123456789)).toEqual(correctFalse);
		expect(val.validateSsn(987654321)).toEqual(correctFalse);
		expect(val.validateSsn('')).toEqual({ valid: false, message: 'Required' });
		expect(val.validateSsn(123412345)).toEqual({ valid: true, message: 'Must be a valid SSN.' });
	});

	it ('should validate no PO box', function(){
		var correctFalse = { valid: false, message: 'Can not contain a P.O. Box' };

		expect(val.validatePoBox('P.O. Box')).toEqual(correctFalse);
		expect(val.validatePoBox('PO Box')).toEqual(correctFalse);
		expect(val.validatePoBox('po box')).toEqual(correctFalse);
		expect(val.validatePoBox('')).toEqual({ valid: false, message: 'Required' });
		expect(val.validatePoBox('Apoboxia Ln.')).toEqual({ valid: true, message: 'Can not contain a P.O. Box' });
		expect(val.validatePoBox('Hippo Boxer St.')).toEqual({ valid: true, message: 'Can not contain a P.O. Box' });
	});

	it ('should sanitize ssn', function() {
		expect(val.sanitizeSsn('112-12-1212')).toBe('112121212');
		expect(val.sanitizeSsn('112 12 1212')).toBe('112121212');
	});

});